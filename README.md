This program is a flask microservice which has a super basic authentication system and read/write key value pair writer. This is a coding challenge for a job. There are LOTS of mistakes, security vulnerabilities, and an abomination to software engineering's best practices. The requirements were to spend a couple hours completing the tasks and it definitely shows. It is functional and satisfies most of the requirements. 

-- Installation
clone this repo and 
pip install -r requirements.txt

-- Running it
python flaskmicroservice.py

It creates a db but you will have to populate it with users. 
