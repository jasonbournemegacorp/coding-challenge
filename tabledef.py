from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///flaskmicro.db', echo=True)
Base = declarative_base()
 
class User(Base):
    __tablename__ = "users"
 
    id = Column(Integer, primary_key=True)
    username = Column(String)
    password = Column(String)
    key_values = relationship("Key_Value", back_populates="user")

    def __init__(self, username, password):
        self.username = username
        self.password = password

class Key_Value(Base):
    __tablename__ = "key_values"
 
    id = Column(Integer, primary_key=True)
    key = Column(String)
    value = Column(String)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User", back_populates="key_values")
 
    def __init__(self, key, value, user_id):
        self.key = key
        self.value = value
        Session = sessionmaker(bind=engine)
        s = Session()
        self.user_id = user_id
        self.User = s.query(User).filter(User.id.in_([user_id])).first()

Base.metadata.create_all(engine)