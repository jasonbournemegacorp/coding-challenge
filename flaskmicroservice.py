from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify
from flask import url_for
import os
import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from tabledef import *

app = Flask(__name__)
 
@app.route('/')
def home(user_id=None):
    if not session.get('logged_in') or not session.get('user_id'):
        return render_template('login.html')
    else:
        session['error'] = False
        return key_values(session.get('user_id'))

@app.route('/login', methods=['POST'])
def login():
    POST_USERNAME = str(request.form['username'])
    POST_PASSWORD = str(request.form['password'])
 
    Session = sessionmaker(bind=engine)
    s = Session()
    result = s.query(User).filter(User.username.in_([POST_USERNAME]), User.password.in_([POST_PASSWORD]) )
    user = result.first()
    if user:
        session['logged_in'] = True
        session['user_id'] = user.id
        session['error'] = False
        return key_values(user.id)
    else:
        session['error'] = True
    
    return home()
    
@app.route('/key_values', methods=['POST', 'GET'])
def key_values(user_id=None):
    Session = sessionmaker(bind=engine)
    s = Session()
    user = s.query(User).filter(User.id.in_([user_id])).first()
    if not user:
        session['logged_in'] = False
        session['user_id'] = False
        return home()

    kvs = user.key_values
    return render_template('key_values.html', key_values=kvs, user_id=user_id)


@app.route('/register', methods=['POST'])
def register():
    Session = sessionmaker(bind=engine)
    s = Session()
    user = User(str(request.form['username']), str(request.form['password']))
    s.add(user)
    session['logged_in'] = True
    session['user_id'] = user.id
    session['error'] = False
    s.commit()
    return key_values(user.id)

 
@app.route("/logout")
def logout():
    session['logged_in'] = False
    session['user_id'] = False
    return home()

@app.route("/api/v1/get/<user_id>", methods=['GET'])
def get_method(user_id):
    # takes a key argument and returns the current value associated with the key if it exists
    key = request.args['key']
    Session = sessionmaker(bind=engine)
    s = Session()
    value = s.query(Key_Value).filter(User.id.in_([user_id]),Key_Value.key.in_([key])).first().value
    try:
        payload = value
        return jsonify(payload)
    except:
        return "Bad key", 400

@app.route("/api/v1/set/<user_id>", methods=['POST'])
def set_method(user_id):
    # The method set takes two arguments, a key and a 
    # value and set the value to the specified value
    key = request.form['key']
    value = request.form['value']
    Session = sessionmaker(bind=engine)
    s = Session()
    kv = Key_Value(key, value, user_id)
    s.add(kv)
    s.commit()
    return 'Key value pair added', 201

if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True,host='0.0.0.0', port=4000)